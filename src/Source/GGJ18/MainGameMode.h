// Copyright 2018 2Bit Studios All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "MainGameMode.generated.h"

UCLASS()
class GGJ18_API AMainGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMainGameMode();
};
