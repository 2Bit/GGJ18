// Copyright 2018 2Bit Studios All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "MainCharacter.generated.h"

UCLASS()
class GGJ18_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<class UActionComponent>> ActionClasses;

	UPROPERTY(VisibleAnywhere, Replicated)
	TArray<class UActionComponent*> Actions;

	UPROPERTY(Replicated)
	int CounterCount;

	UPROPERTY(Replicated)
	int StunCount;

public:
	AMainCharacter();

	virtual void BeginPlay() override;
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	UFUNCTION(BlueprintCallable)
	void BeginSprint();

	UFUNCTION(BlueprintCallable)
	void EndSprint();

	UFUNCTION(BlueprintCallable)
	bool IsSprinting() const;

	UFUNCTION(BlueprintCallable)
	void SetCounter();

	UFUNCTION(BlueprintCallable)
	void UnsetCounter();

	UFUNCTION(BlueprintCallable)
	bool IsCountering() const;

	UFUNCTION(BlueprintCallable)
	void SetStun();

	UFUNCTION(BlueprintCallable)
	void UnsetStun();

	UFUNCTION(BlueprintCallable)
	bool IsStunned() const;

	UFUNCTION(BlueprintCallable)
	int GetActionsCount() const;

	UFUNCTION(Blueprintcallable)
	class UActionComponent* GetAction(int Index);

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
	void Server_UseAction(int Index);

	UFUNCTION(BlueprintCallable)
	class AStatus* ApplyStatus(TSubclassOf<class AStatus> StatusClass, float Duration);

	UFUNCTION(BlueprintCallable)
	void RemoveStatus(TSubclassOf<class AStatus> StatusClass);

	UFUNCTION(BlueprintCallable)
	AActor* SpawnAssociatedActor(TSubclassOf<AActor> Class, FVector Location, FRotator Rotation);
};
