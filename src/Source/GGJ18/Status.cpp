// Copyright 2018 2Bit Studios All Rights Reserved.

#include "Status.h"

#include "MainCharacter.h"

AStatus::AStatus()
{
	this->PrimaryActorTick.bCanEverTick = true;

	this->bReplicates = true;
}

void AStatus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	this->DurationRemaining -= DeltaTime;

	if (this->DurationRemaining <= 0.0f)
	{
		this->Remove();

		this->Destroy();
	}
}

void AStatus::ApplyTo(AMainCharacter* Target, float Duration)
{
	this->Remove();

	this->AttachToActor(Target, FAttachmentTransformRules::SnapToTargetIncludingScale);

	this->CurrentTarget = Target;
	this->DurationRemaining = Duration;

	this->SetApply(this->CurrentTarget);
}

void AStatus::Remove()
{
	if (this->CurrentTarget != nullptr)
	{
		this->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);

		AMainCharacter* OldTarget = this->CurrentTarget;
		this->CurrentTarget = nullptr;

		this->UnsetApply(OldTarget);
	}
}

float AStatus::GetDurationRemaining() const
{
	return this->DurationRemaining;
}

AMainCharacter* AStatus::GetCurrentTarget() const
{
	return this->CurrentTarget;
}
