// Copyright 2018 2Bit Studios All Rights Reserved.

#include "MainCharacter.h"

#include "GameFramework/CharacterMovementComponent.h"

#include "ActionComponent.h"
#include "Status.h"

#include "UnrealNetwork.h"
#include "Engine/ActorChannel.h"

AMainCharacter::AMainCharacter()
{
	this->PrimaryActorTick.bCanEverTick = true;

	this->bUseControllerRotationYaw = true;
}

void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (this->HasAuthority())
	{
		for (int i = 0; i < this->ActionClasses.Num(); ++i)
		{
			UActionComponent* const NewAction = NewObject<UActionComponent>(this, *this->ActionClasses[i]);
			NewAction->RegisterComponent();

			this->Actions.Add(NewAction);
		}
	}
}

bool AMainCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int i = 0; i < this->Actions.Num(); ++i)
	{
		WroteSomething |= Channel->ReplicateSubobject(this->Actions[i], *Bunch, *RepFlags);
	}

	return WroteSomething;
}

void AMainCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMainCharacter, Actions);
	DOREPLIFETIME(AMainCharacter, CounterCount);
	DOREPLIFETIME(AMainCharacter, StunCount);
}

void AMainCharacter::BeginSprint()
{
	this->Crouch();
}

void AMainCharacter::EndSprint()
{
	this->UnCrouch();
}

bool AMainCharacter::IsSprinting() const
{
	return this->bIsCrouched;
}

void AMainCharacter::SetCounter()
{
	++this->CounterCount;
}

void AMainCharacter::UnsetCounter()
{
	--this->CounterCount;
}

bool AMainCharacter::IsCountering() const
{
	return this->CounterCount > 0;
}

void AMainCharacter::SetStun()
{
	++this->StunCount;
}

void AMainCharacter::UnsetStun()
{
	--this->StunCount;
}

bool AMainCharacter::IsStunned() const
{
	return this->StunCount > 0;
}

int AMainCharacter::GetActionsCount() const
{
	return this->Actions.Num();
}

UActionComponent* AMainCharacter::GetAction(int Index)
{
	if (Index >= 0 && Index < this->Actions.Num())
	{
		return this->Actions[Index];
	}

	return nullptr;
}

bool AMainCharacter::Server_UseAction_Validate(int Index)
{
	return true;
}

void AMainCharacter::Server_UseAction_Implementation(int Index)
{
	if (Index >= 0 && Index < this->Actions.Num())
	{
		this->Actions[Index]->Use(this);
	}
}

AStatus* AMainCharacter::ApplyStatus(TSubclassOf<AStatus> StatusClass, float Duration)
{
	AStatus* Status = this->GetWorld()->SpawnActor<AStatus>(*StatusClass);
	
	Status->ApplyTo(this, Duration);

	return Status;
}

void AMainCharacter::RemoveStatus(TSubclassOf<AStatus> StatusClass)
{
	TArray<AActor*> AttachedActors;
	this->GetAttachedActors(AttachedActors);

	for (int i = 0; i < AttachedActors.Num(); ++i)
	{
		AStatus* const AttachedStatus = Cast<AStatus>(AttachedActors[i]);
		if (AttachedStatus != nullptr && AttachedStatus->GetClass() == StatusClass)
		{
			AttachedStatus->Remove();
			AttachedStatus->Destroy();

			break;
		}
	}
}

AActor* AMainCharacter::SpawnAssociatedActor(TSubclassOf<AActor> Class, FVector Location, FRotator Rotation)
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Owner = this;
	SpawnParameters.Instigator = this;

	AActor* AssociatedActor = this->GetWorld()->SpawnActor<AActor>(*Class, Location, Rotation, SpawnParameters);

	return AssociatedActor;
}
