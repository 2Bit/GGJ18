// Copyright 2018 2Bit Studios All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "ActionComponent.generated.h"

UCLASS(Blueprintable, Abstract)
class GGJ18_API UActionComponent : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float Cooldown;

public:
	UActionComponent();

	virtual bool IsSupportedForNetworking() const override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const;

	UFUNCTION(BlueprintCallable)
	bool Use(class AMainCharacter* User);

	UFUNCTION(BlueprintCallable)
	float GetCooldownRemaining() const;

	UFUNCTION(BlueprintCallable)
	bool IsCoolingDown() const;

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void PerformUse(class AMainCharacter* User);

private:
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Multicast_StartCooldown();

	UPROPERTY()
	float LastUseTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UTexture2D* Icon;

public:
	FORCEINLINE class UTexture2D* GetIcon()
	{
		return this->Icon;
	}
};
