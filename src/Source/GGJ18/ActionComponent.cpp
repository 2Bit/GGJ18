// Copyright 2018 2Bit Studios All Rights Reserved.

#include "ActionComponent.h"

#include "MainCharacter.h"

#include "UnrealNetwork.h"

UActionComponent::UActionComponent()
{
	this->Cooldown = 1.0f;
}

bool UActionComponent::IsSupportedForNetworking() const
{
	return true;
}

void UActionComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

bool UActionComponent::Use(AMainCharacter* User)
{
	if (this->IsCoolingDown() || User->IsStunned() || User->IsSprinting())
	{
		return false;
	}

	this->Multicast_StartCooldown();

	this->PerformUse(User);

	return true;
}

float UActionComponent::GetCooldownRemaining() const
{
	float const TimeSinceLastUse = this->GetWorld()->TimeSeconds - this->LastUseTime;

	return FMath::Max(0.0f, this->Cooldown - TimeSinceLastUse);
}

bool UActionComponent::IsCoolingDown() const
{
	return this->GetCooldownRemaining() > 0.0f;
}

bool UActionComponent::Multicast_StartCooldown_Validate()
{
	return true;
}

void UActionComponent::Multicast_StartCooldown_Implementation()
{
	this->LastUseTime = this->GetWorld()->TimeSeconds;
}
