// Copyright 2018 2Bit Studios All Rights Reserved.

#include "MainPlayerController.h"

#include "MainCharacter.h"

AMainPlayerController::AMainPlayerController()
{
	this->BaseTurnRate = 45.0f;
	this->BaseLookRate = 45.0f;
}

void AMainPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	this->InputComponent->BindAction("Jump", IE_Pressed, this, &AMainPlayerController::BeginJump);
	this->InputComponent->BindAction("Jump", IE_Released, this, &AMainPlayerController::EndJump);

	this->InputComponent->BindAction("Sprint", IE_Pressed, this, &AMainPlayerController::BeginSprint);
	this->InputComponent->BindAction("Sprint", IE_Released, this, &AMainPlayerController::EndSprint);

	this->InputComponent->BindAction("Action1", IE_Pressed, this, &AMainPlayerController::UseAction1);
	this->InputComponent->BindAction("Action2", IE_Pressed, this, &AMainPlayerController::UseAction2);
	this->InputComponent->BindAction("Action3", IE_Pressed, this, &AMainPlayerController::UseAction3);

	this->InputComponent->BindAxis("Forward", this, &AMainPlayerController::MoveForward);
	this->InputComponent->BindAxis("Right", this, &AMainPlayerController::MoveRight);

	this->InputComponent->BindAxis("Turn", this, &AMainPlayerController::Turn);
	this->InputComponent->BindAxis("TurnRate", this, &AMainPlayerController::TurnRate);
	this->InputComponent->BindAxis("Look", this, &AMainPlayerController::Look);
	this->InputComponent->BindAxis("LookRate", this, &AMainPlayerController::LookRate);
}

void AMainPlayerController::BeginJump()
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr && !MainCharacter->IsStunned())
	{
		this->GetCharacter()->Jump();
	}
}

void AMainPlayerController::EndJump()
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr)
	{
		this->GetCharacter()->StopJumping();
	}
}

void AMainPlayerController::BeginSprint()
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr)
	{
		MainCharacter->BeginSprint();
	}
}

void AMainPlayerController::EndSprint()
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr)
	{
		MainCharacter->EndSprint();
	}
}

void AMainPlayerController::UseAction1()
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr)
	{
		MainCharacter->Server_UseAction(0);
	}
}

void AMainPlayerController::UseAction2()
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr)
	{
		MainCharacter->Server_UseAction(1);
	}
}

void AMainPlayerController::UseAction3()
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr)
	{
		MainCharacter->Server_UseAction(2);
	}
}

void AMainPlayerController::MoveForward(float Value)
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr && !MainCharacter->IsStunned())
	{
		this->GetCharacter()->AddMovementInput(this->GetCharacter()->GetActorForwardVector(), Value);
	}
}

void AMainPlayerController::MoveRight(float Value)
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr && !MainCharacter->IsStunned())
	{
		this->GetCharacter()->AddMovementInput(this->GetCharacter()->GetActorRightVector(), Value);
	}
}

void AMainPlayerController::Turn(float Value)
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr && !MainCharacter->IsStunned())
	{
		this->AddYawInput(Value);
	}
}

void AMainPlayerController::TurnRate(float Value)
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr && !MainCharacter->IsStunned())
	{
		this->AddYawInput(Value * this->BaseTurnRate * this->GetWorld()->GetDeltaSeconds());
	}
}

void AMainPlayerController::Look(float Value)
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr && !MainCharacter->IsStunned())
	{
		this->AddPitchInput(Value);
	}
}

void AMainPlayerController::LookRate(float Value)
{
	AMainCharacter* MainCharacter = Cast<AMainCharacter>(this->GetCharacter());
	if (MainCharacter != nullptr && !MainCharacter->IsStunned())
	{
		this->AddPitchInput(Value * this->BaseLookRate * this->GetWorld()->GetDeltaSeconds());
	}
}
