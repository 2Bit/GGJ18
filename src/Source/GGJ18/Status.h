// Copyright 2018 2Bit Studios All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Status.generated.h"

UCLASS(Abstract)
class GGJ18_API AStatus : public AActor
{
	GENERATED_BODY()

public:
	AStatus();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void ApplyTo(class AMainCharacter* Target, float Duration);

	UFUNCTION(BlueprintCallable)
	void Remove();

	UFUNCTION(BlueprintCallable)
	float GetDurationRemaining() const;

	UFUNCTION(BlueprintCallable)
	class AMainCharacter* GetCurrentTarget() const;

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void SetApply(class AMainCharacter* Target);

	UFUNCTION(BlueprintImplementableEvent)
	void UnsetApply(class AMainCharacter* Target);

private:
	UPROPERTY()
	float DurationRemaining;

	UPROPERTY()
	class AMainCharacter* CurrentTarget;
};
