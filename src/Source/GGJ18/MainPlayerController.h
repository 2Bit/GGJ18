// Copyright 2018 2Bit Studios All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "MainPlayerController.generated.h"

UCLASS()
class GGJ18_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float BaseTurnRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float BaseLookRate;

public:
	AMainPlayerController();

	virtual void SetupInputComponent() override;

	void BeginJump();
	void EndJump();

	void BeginSprint();
	void EndSprint();

	void UseAction1();
	void UseAction2();
	void UseAction3();

	void MoveForward(float Value);
	void MoveRight(float Value);

	void Turn(float Value);
	void TurnRate(float Value);
	void Look(float Value);
	void LookRate(float Value);
};
